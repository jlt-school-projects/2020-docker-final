const { promisify } = require("util");

module.exports = async (app, redis) => {
    const redisGetAsync = promisify(redis.get).bind(redis);
    const redisIncrAsync = promisify(redis.incr).bind(redis);
    app.get('/',async (req, res) => {
        await redisIncrAsync('visitors')
        const visitors = await redisGetAsync("visitors");
        res.send(`You are visitor ${visitors}`);
    });
};
