const express = require('express');
const app = express();
require('dotenv').config();

(async () => {
    const db = await require('./config/database/database')();
    const redis = await require('./config/redis/redis');

    require('./endpoints/put_name')(app,db);
    require('./endpoints/main')(app,redis);

    const server = app.listen(8080);
})();
