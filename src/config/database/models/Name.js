const sequelize = require('sequelize');

module.exports = (db) => {
    const Name = db.define('name', {
        name : { type: sequelize.DataTypes.STRING },
    });
    return Name;
};